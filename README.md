# Frequency controller

Frequency controller (digital PID) of a quartz-crystal with the 1PPS GPS signal and an ATMega32U4 µcontroler.

* pcb design with [KiCAD](https://www.kicad.org/)
* simulation for optimisation and caracterzation with [NGSpice](http://ngspice.sourceforge.net/)

Main is `regulateur.c`
