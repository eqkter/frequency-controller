#define F_CPU 16000000UL
#include <avr/io.h>
#include <avr/power.h>
#include <avr/interrupt.h>

void pwm_setup(float a){ // sur timer 4
	TCCR4B = (1 << CS40); // prescale 1
	TCCR4C = (1 << PWM4D) | (1 << COM4D1); // PWM, écoute sur OC4D
	TCCR4D = 0; // 00 -> Fast PWM
	
	DDRD |= (1 << PORTD7); // D7/OC4D
	OCR4C = 0xff; // période du créneau
	OCR4D = (int)((float)(0xff) * a); // largeur du créneau, 0 < a < 1
}
