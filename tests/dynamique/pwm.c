#define F_CPU 16000000UL
#include <avr/io.h>
#include <avr/power.h>
#include <avr/interrupt.h>

void pwm_setup(float a){ // sur timer 4
	TCCR4B = (1 << CS40); // prescale 1024
	TCCR4C = (1 << PWM4D) | (1 << COM4D1); // PWM, écoute sur OCR4D
	//TCCR4D |= (1 << WGM41) | (1 << WGM40); // 00 -> Fast PWM
	
	DDRD |= (1 << PORTD7); // D7/OC4D
	OCR4C = 0xff; // période du créneau
	OCR4D = (int)((float)(0xff) * a); // largeur du créneau (τ = 50%)
}
