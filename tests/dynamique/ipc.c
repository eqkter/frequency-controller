#define F_CPU 16000000UL
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/power.h>

void icp_setup(void){ // initialisation timer1
	TCCR1A = 0; // OVF
	TCCR1B |= (1 << ICES1) | (1 << CS11); // prescale de 8, rising edge
	TIFR1 = (1 << ICF1); // IC
	TIMSK1 = (1 << ICIE1) | (1 << TOIE1); // interrupts IC OVF
}
