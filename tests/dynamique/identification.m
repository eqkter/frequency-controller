% Écart de 32 ticks en échelon
% Retard de 2s -> a = 16 ech/s pour Takahashi

f = fopen("mesures.dat");
d = fscanf(f, "%x%x%d");

OVF = d(1:3:end);
r = d(2:3:end);
a = d(3:3:end);

F = 16e6; % Hz
p = 8; % prescale
err = r;
%err = ((2^16) * OVF + r) * p - F; 
err = err(1:end-1);

N = floor(length(r) / 30) - 1;
s = reshape(err(24:N*30+23), 30, N);

figure(1), hold on, grid();
plot([-15:14], s)
xlabel("Temps (s)")
ylabel("Écart en fréquence (Hz)")

figure(2), hold on, grid();
plot([-15:14], mean(s'))
xlabel("Temps (s)")
ylabel("Écart en fréquence (Hz)")

pause()
