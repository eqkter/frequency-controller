f3 = fopen("mesuresPID.dat");
d3 = fscanf(f3, "%x%x%d");
r3 = d3(2:3:end);
c3 = d3(3:3:end);

f2 = fopen("mesuresPI.dat");
d2 = fscanf(f2, "%x%x%d");
r2 = d2(2:3:end);
c2 = d2(3:3:end);

f = fopen("mesuresNCorr.dat");
d = fscanf(f, "%x%x%d");
r= d(2:3:end);

size(r)
size(r2)
consigne = 34100;

figure(1), grid();
subplot(3,1,1)
plot(r(4:end) - consigne, '*'), xlabel("Temps (s)"), ylabel("Écart de consigne (tics)"), title("Non corrigé");
subplot(3,1,2)
plot(r2(6:end) - consigne, 'r*'), xlabel("Temps (s)"), ylabel("Écart de consigne (tics)"), title("PI");
subplot(3,1,3)
plot(r3(6:end) - consigne, 'm*'), xlabel("Temps (s)"), ylabel("Écart de consigne (tics)"), title("PID");

figure(2), grid();
subplot(2,1,1)
plot(c2(4:end).+30, 'r*'), xlabel("Temps (s)"), ylabel("Correction : valeur PWM (%*5V)"), title("PI");
subplot(2,1,2)
plot(c3(4:end).+30, 'm*'), xlabel("Temps (s)"), ylabel("Correction : valeur PWM (%*5V)"), title("PID");

pause()
