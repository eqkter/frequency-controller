#define F_CPU 16000000UL
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#include <util/delay.h>

#include "serial.h"
#include "ipc.h"
#include "pwm.h"
#include "communication.h"

volatile unsigned short res = 0;
volatile unsigned short ovf = 0;

extern USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface;
extern FILE USBSerialStream;

void init(void);
void mesure(float, unsigned short *);
void commande(float);
void sature(float *);

ISR(TIMER1_CAPT_vect){
	res = ICR1;
	TCNT1 = 0;
}

ISR(TIMER1_OVF_vect){ovf++;}

int main (void){
	float duty = 0.3;
	unsigned short freq;
       	short erreur, erreur_pre, erreur_pre_pre;
	unsigned short consigne = 34100;
	float corr = 0.0;
	float Ki = 7e-3 / 3; // essai-erreur
	float Kp = 5e-3 / 2;
	float Kd = 0.0; // 50e-3 / 3 --> très faible
	
	init();
	icp_setup();
	pwm_setup(duty);
	sei();
	
	erreur = 0;
	erreur_pre = 0;
	erreur_pre_pre = 0;
	while (1){
		if (res != 0) {
			mesure(corr, &freq); 
			erreur_pre_pre = erreur_pre;
			erreur_pre = erreur;
			erreur = consigne - freq;
			corr += Kp * (float)(erreur - erreur_pre) + Ki * (float)erreur + Kd * (erreur + erreur_pre_pre + 2*erreur_pre); 
			sature(&corr);
			commande(duty + corr);
		}
	}
	return 0;
}

void init(void){
	// LED
	MCUCR |= (1 << JTD);
	MCUCR |= (1 << JTD);
	DDRF |= (1 << PORTF1); // 1PPS
	DDRF |= (1 << PORTF6); // PWM ON
	PORTF &= ~(1 << PORTF1);
	PORTF |= (1 << PORTF6);

	SetupHardware();
	CDC_Device_CreateStream(&VirtualSerial_CDC_Interface, &USBSerialStream);
	GlobalInterruptEnable();
}

void mesure(float a, unsigned short *f){
	fputs("0x", &USBSerialStream); // Nb OVF
	write_short(ovf); //0x0018=24
	fputs("\t0x", &USBSerialStream); write_short(res); // erreur
	fprintf(&USBSerialStream, "\t%d\r\n", (int)(100.0 * a));		
	*f = res;
	res = 0;
	ovf = 0;
	
	PORTF |= (1 << PORTF1); // LED à 1PPS
	_delay_ms(100);
	PORTF &= ~(1 << PORTF1);
	
	CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);
	CDC_Device_USBTask(&VirtualSerial_CDC_Interface);
	USB_USBTask();
}

void commande(float a){OCR4D = (int)((float)(OCR4C) * a);}

void sature(float *c){
	if (*c > 0.6) {*c = 0.6;}
	if (*c < -0.3) {*c = -0.3;}
}
