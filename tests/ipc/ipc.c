#define F_CPU 16000000UL
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/power.h>
#include <util/delay.h>

#include "serial.h"

volatile unsigned short res = 0;
volatile unsigned short ovl = 0;

extern USB_ClassInfo_CDC_Device_t VirtualSerial_CDC_Interface;
extern FILE USBSerialStream;

void icp_setup(void) // initialisation timer3
{	TCCR1A = 0; // OVF
	TCCR1B |= (1<<ICES1) | (1<<CS11); // prescale ovf
	TIFR1 = 1 << ICF1; // IC
	TIMSK1 = (1 << ICIE1) | (1 << TOIE1);
}

void transmit_data(uint8_t data){
	char buffer[2];
	buffer[0] = data; buffer[1] = 0;
	fputs(buffer, &USBSerialStream);
}

void write_char(unsigned char c)
{if ((c>>4) > 9) transmit_data((c >> 4) - 10 + 'A'); else transmit_data((c >> 4) + '0');
if ((c & 0x0f) > 9) transmit_data((c & 0x0f) - 10 + 'A'); else transmit_data((c & 0x0f) + '0');
}

void write_short(unsigned short s){
	write_char(s >> 8);
	write_char(s & 0xff);
}

ISR(TIMER1_CAPT_vect){
	res = ICR1;
	TCNT1 = 0;
}

ISR(TIMER1_OVF_vect){ovl++;}

int main (void){
	// LED
	MCUCR|=(1<<JTD);
	MCUCR|=(1<<JTD);
	DDRF |=(1<<PORTF1);
	PORTF&=~(1<<PORTF1);

	SetupHardware();
	CDC_Device_CreateStream(&VirtualSerial_CDC_Interface, &USBSerialStream);
	GlobalInterruptEnable();
	icp_setup();
	sei();
	
	while (1){
		if (res != 0) {
			//fputs("0x", &USBSerialStream); // Nb OVF
			//write_short(ovl); //0x0018=24
			fputs("0x", &USBSerialStream);
			write_short(res); // erreur
			fputs("\r\n\0", &USBSerialStream);
			_delay_ms(100);
			PORTF |= (1 << PORTF1); // LED à 1PPS
			_delay_ms(100);
			PORTF &= ~(1 << PORTF1);
			res = 0;
			ovl = 0;
		}

		CDC_Device_ReceiveByte(&VirtualSerial_CDC_Interface);
		CDC_Device_USBTask(&VirtualSerial_CDC_Interface);
		USB_USBTask();
	}
	return 0;
}
