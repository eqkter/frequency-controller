n = 1;
vC = 1:1:20;
for c = vC
	f = fopen("capapieds.cir", "w") ; % netlist
	fprintf(f, "* tirage resonateur \n");
	fprintf(f, "V1 99 0 1.0 ac 1.0 \n"); % source
	fprintf(f, "rs 99 1 100k \n"); % src Z
	fprintf(f, "R1 1 2 22.8 \n") ; % BVD motional
	fprintf(f, "L1 2 3 70.3m \n");
	fprintf(f, "C1 3 4 1.4f \n");
	fprintf(f, "C0 1 4 0.65p \n"); % BVD elec
	fprintf(f, "Cp1 1 0 20p \n") ; % capa pied 1
	fprintf(f, "Cp2 4 0 %fp \n", c) ; % capa pied 2 + varicap
	fprintf(f, "RL 4 0 100k \n"); % load
	fprintf(f, ".control \n");	
	fprintf(f, "ac lin 5000 16.02meg 16.08meg \n"); % Bode
	fprintf(f, "print vdb(4) vp(4) > t \n"); % plot
	fprintf(f, "quit \n") ; % 1=line, 2=freq, 3=dB, 4=ph
	fprintf(f, ".endc \n");
	fclose(f);
		
	system("ngspice capapieds.cir > /dev/null");
	system("cat t | grep ^[0-9] t > tt");
	system("rm t") ;
	load tt
	[a, b] = max(tt(:,3)); % recherche max admittance
	solution(n) = tt(b, 2); % 2eme colonne=frequence
	n = n + 1;
end

figure()
plot(vC, solution ./ 1e6)
xlabel("Varicap C (pF)")
ylabel("Fréquence oscillateur (MHz)")

pause()
