.subckt opamp 1 2 3
* +in (1) -in (2) out (3)
rin 1 2 2meg
e 4 0 1 2 100k
rbw 4 5 500k
cbw 5 0 31.85n
eout 6 0 5 0 1
rout 6 3 75
.ends opamp
